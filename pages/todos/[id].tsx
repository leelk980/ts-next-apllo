import { NextPage } from 'next';
import { useQuery, gql } from '@apollo/client';
import { MouseEventHandler, useEffect, useState } from 'react';
import { useRouter } from 'next/dist/client/router';

const GET_TODO = gql`
  query TODO($id: ID!) {
    todo(id: $id) {
      id
      title
      completed
      user {
        name
        email
      }
    }
  }
`;

type Todo = {
  id: number;
  title: string;
  completed: string;
  user: {
    name: string;
    email: string;
  };
};

const TodoPage: NextPage<any> = () => {
  const router = useRouter();
  const { id } = router.query;
  const todoId: number = +id!;

  const { data, loading, error, refetch } = useQuery(GET_TODO, { variables: { id: todoId } });

  const [todo, setTodo] = useState<Todo>();

  useEffect(() => {
    setTodo(data?.todo);
  }, [data]);

  const clickPrevious: MouseEventHandler<HTMLButtonElement> = async (e) => {
    e.preventDefault();
    if (todoId === 1) {
      return alert('첫번째 Todo 입니다.');
    }
    router.push(`/todos/${todoId - 1}`);
  };

  const clickNext: MouseEventHandler<HTMLButtonElement> = async (e) => {
    e.preventDefault();

    router.push(`/todos/${todoId + 1}`);
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error...</div>;
  }

  return (
    <main>
      <h1>Todo</h1>
      <div>id: {todo?.id}</div>
      <div>title: {todo?.title}</div>
      <div>completed: {todo?.completed ? 'true' : 'false'}</div>
      <div>username: {todo?.user.name}</div>
      <div>email: {todo?.user.email}</div>
      <button onClick={clickPrevious}>previous</button>
      <button onClick={clickNext}>next</button>
    </main>
  );
};

export default TodoPage;

import { GetServerSideProps, NextPage } from 'next';
import { initializeApollo } from '../../lib/apolloClient';
import { useQuery, gql } from '@apollo/client';
import { MouseEventHandler, useState } from 'react';

const GET_TODOS = gql`
  query TODOS($page: Int!, $limit: Int!) {
    todos(options: { paginate: { page: $page, limit: $limit } }) {
      data {
        id
        title
        completed
      }
    }
  }
`;

type Todo = {
  id: number;
  title: string;
  completed: string;
};

const PostPage: NextPage = () => {
  const { data, error, refetch } = useQuery(GET_TODOS, { variables: { page: 1, limit: 10 } });

  const [todos, setTodos] = useState<Todo[]>(data.todos.data);
  const [currentPage, setCurrentPage] = useState<number>(1);

  const clickPrevious: MouseEventHandler<HTMLButtonElement> = async () => {
    if (currentPage === 1) {
      return alert('첫번째 페이지 입니다.');
    }

    const { data } = await refetch({ page: currentPage - 1, limit: 10 });

    setCurrentPage((prev) => prev - 1);
    setTodos(data.todos.data);
  };

  const clickNext: MouseEventHandler<HTMLButtonElement> = async () => {
    const { data } = await refetch({ page: currentPage + 1, limit: 10 });

    setCurrentPage((prev) => prev + 1);
    setTodos(data.todos.data);
  };

  if (error) {
    return <div>Error...</div>;
  }

  return (
    <main>
      <h1>Todo List</h1>
      {todos.map(({ id, title, completed }) => (
        <div>
          <span> id: {id},</span>
          <span> title: {title},</span>
          <span> completed: {completed ? 'true' : 'false'}</span>
          <div>--------</div>
        </div>
      ))}
      <div>
        <button onClick={clickPrevious}>previous</button>
        <button onClick={clickNext}>next</button>
      </div>
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: GET_TODOS,
    variables: {
      page: 1,
      limit: 10,
    },
  });

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
  };
};

export default PostPage;
